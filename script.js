// declare variable
var urlAPI = "http://ps-mw.pt.fuchu.toshiba.co.jp/psweb/cgi-bin/license.cgi";
var passwd = document.getElementById("passwd");
var myname = document.getElementById("myname");
var busho = document.getElementById("busho");
var mailaddress = document.getElementById("mailaddress");
var sysnam = document.getElementById("sysnam");
var basyo = document.getElementById("basyo");
var jyotyo = document.getElementById("jyotyo");
var hostid = document.getElementById("hostid");
var cpuno = document.getElementById("cpuno");
var sirius2 = document.getElementById("sirius2");
var sirius3 = document.getElementById("sirius3");
var gremcast3 = document.getElementById("gremcast3");
var giproma3 = document.getElementById("giproma3");
var giproma4 = document.getElementById("giproma4");
var gremcastLite1 = document.getElementById("gremcastLite1");
var gipromaLite1 = document.getElementById("gipromaLite1");
var opecon2 = document.getElementById("opecon2");
var olive1 = document.getElementById("olive1");
var guiruntime1 = document.getElementById("guiruntime1");
var stdccs1 = document.getElementById("stdccs1");
// request flag (0: do not request yet; 1: 1 time request)
var reqflg = 0;
// success flag (0: error; 1: success)
var sucflg = 1;

// declare method
function sendreq(){
    var hostlst = hostid.value.split('\n');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var restxt = this.responseText;
            
            console.log("in response, success flag: " + sucflg);
            if(reqflg === 0){
                console.log(restxt);
                reqflg = 1;                
                sucflg = restxt.indexOf("失敗") !== -1 ? 0 : 1;
                
                document.body.innerHTML = restxt;
            }
        }
    };

    for( var i=0; i<hostlst.length; i++) {
        xhttp.open("POST", urlAPI, false);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.overrideMimeType("text/html; charset=EUC-JP");
        
        var formData = data( hostlst[i] );
        let urlEncodedData = "",
            urlEncodedDataPairs = [],
            name;

        // Turn the data object into an array of URL-encoded key/value pairs.
        for( name in formData ) {
            urlEncodedDataPairs.push( encodeURIComponent( name ) + '=' + EscapeEUCJP( formData[name] ) );
        }
        
        // Combine the pairs into a single string and replace all %-encoded spaces to 
        // the '+' character; matches the behaviour of browser form submissions.
        urlEncodedData = urlEncodedDataPairs.join( '&' ).replace( /%20/g, '+' );
        
        xhttp.send(urlEncodedData);
        
        if(sucflg === 0) break;
    }
}

function data(host){
    var formData = {
    "passwd":  passwd.value,
    "myname":  myname.value,
    "busho":  busho.value,
    "mailaddress":  mailaddress.value,
    "sysnam":  sysnam.value,
    "basyo":  basyo.value,
    "jyotyo":  jyotyo.value,
    "hostid":  host,
    "cpuno":  cpuno.value
    };
    if(sirius2.checked) formData["sirius2"] = sirius2.value;
    if(sirius3.checked) formData["sirius3"] = sirius3.value;
    if(gremcast3.checked) formData["gremcast3"] = gremcast3.value;
    if(giproma3.checked) formData["giproma3"] = giproma3.value;
    if(giproma4.checked) formData["giproma4"] = giproma4.value;
    if(gremcastLite1.checked) formData["gremcastLite1"] = gremcastLite1.value;
    if(gipromaLite1.checked) formData["gipromaLite1"] = gipromaLite1.value;
    if(opecon2.checked) formData["opecon2"] = opecon2.value;
    if(olive1.checked) formData["olive1"] = olive1.value;
    if(guiruntime1.checked) formData["guiruntime1"] = guiruntime1.value;
    if(stdccs1.checked) formData["stdccs1"] = stdccs1.value;
    
    return formData;
}